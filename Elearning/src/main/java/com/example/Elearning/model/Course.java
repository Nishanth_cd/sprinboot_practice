package com.example.Elearning.model;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "Courses")
public class Course {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name= "title")
    private String title;
    @Column(name= "instructors")
    private String instructors;
    @Column(name= "start_date")
    private String start_date;
    @Column(name= "end_date")
    private String end_date;



}
