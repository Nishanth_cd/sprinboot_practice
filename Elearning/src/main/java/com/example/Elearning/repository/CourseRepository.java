package com.example.Elearning.repository;


import com.example.Elearning.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

    public Course findByTitleAndInstructors(String title, String instructors);


    @Query(value = "select title from Courses where title=?", nativeQuery = true)
    public String findByTitle (String title);

    @Query(value = "select * from Courses", nativeQuery = true)
    public List<Course> findAll();

    @Query(value = "select * from Courses where id=?", nativeQuery = true)
    public Course findById (int id);


}



