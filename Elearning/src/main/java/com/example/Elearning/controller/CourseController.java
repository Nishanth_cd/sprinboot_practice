package com.example.Elearning.controller;


import com.example.Elearning.model.Course;
import com.example.Elearning.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class CourseController {

    @Autowired
    private CourseService courseService;

    //fetch all courses from DB
    @GetMapping("/courses")
    public List<Course> getAllCourse(){
        return courseService.listAll();
    }

    //fetch course details by id
    @RequestMapping(value = "course/{id}", method = RequestMethod.GET)
    public Course getCourse(@PathVariable("id") int id){
        return courseService.getCourseById(id);
    }


    @RequestMapping(value = "course/name/{title}", method = RequestMethod.GET)
    public String findBytitle(@PathVariable("title") String title) {

        return  courseService.findByTitle(title);
    }

    //To insert new Course into DB
    @RequestMapping (value = "/course", method = RequestMethod.POST)
    public  String datas ( @RequestBody Course data){
        System.out.println(data.getInstructors());
        return data.getInstructors();
    }
}
