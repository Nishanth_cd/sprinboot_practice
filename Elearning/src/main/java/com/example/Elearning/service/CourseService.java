package com.example.Elearning.service;


import com.example.Elearning.model.Course;
import com.example.Elearning.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {


    @Autowired
    private CourseRepository repo;


    //Get all course details present in the DB
    public List<Course> listAll() {
        System.out.println(repo.findAll());
        return repo.findAll();

    }

    //Getting Course details by id
    public Course getCourseById(int id)
    {
        System.out.println(repo.findById(id));
        Course course = repo.findById (id);
        return course;
    }

    //Just for title
    public String findByTitle(String title)
    {
        return repo.findByTitle(title);
    }
}
